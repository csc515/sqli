package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name="REVIEWS")
public class Review {
	//@Id
	//@Column(name = "id", columnDefinition = "INTEGER")
	private Integer id;
	
	//@Column(name = "name", columnDefinition = "VARCHAR(100)")
	private String name;
	
	//@Column(name = "review", columnDefinition = "VARCHAR(255)")
	private String review;
	
	//@Column(name = "country", columnDefinition = "VARCHAR(255)")
	private String country;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReview() {
		return review;
	}

	public void setReview(String review) {
		this.review = review;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
