package edu.missouristate.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Review;
import edu.missouristate.repository.SqliRepository;

@Service("sqliService")
public class SqliService {
	
	@Autowired
	SqliRepository sqliRepo;
	
	public Boolean isAuthenticated(String username, String password) {
		return sqliRepo.isAuthenticated(username, password); 
	}

	public List<Review> getReviewList() {
		return sqliRepo.getReviewList();
	}
	
}
